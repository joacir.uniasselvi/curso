const calculadora = require('./modulo.01')();
const diadia = require('./modulo.02');
const cafe = require('./modulo.03');
const contato1 = require('./contatos')();
const contato2 = require('./contatos')();

console.log(diadia.despertar('Zica'));
console.log('O que temos para o café da manhã?\nR:', cafe.prepararCafeDaManha(['Pão', 'Suco de Limão']));
console.log("2 + 2 =", calculadora.somar(2, 2));

console.log('Total de contatos: ', contato1.getContatos());
console.log('Total de contatos: ', contato2.getContatos());

contato1.add('João');

console.log('Total de contatos: ', contato1.getContatos());
console.log('Total de contatos: ', contato2.getContatos());